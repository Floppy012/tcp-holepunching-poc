#!/usr/bin/python
'''
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2019 Florian Schmidt <hi@f012.dev>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

import socket
import time



srvsock = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)

srvsock.connect(("116.203.141.5", 6001))
#srvsock.connect(("127.0.0.1", 6001))
srvsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
srvsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

priv_addr = srvsock.getsockname()

while True:
    print "waiting ..."
    recv = srvsock.recv(8192)
    print recv
    if ":" in recv:
        addr = recv.split(':')
        ip = addr[0]
        port = int(addr[1])
        combine = (ip, port)
        if addr[2] == "S":
            print "I'm the server!"
            print "PRIV_ADDR: %s:%s" % priv_addr
            servsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            servsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            servsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
            servsock.bind(('', priv_addr[1]))
            servsock.listen(1)
            servsock.settimeout(5)
            while True:
                try:
                    conn, addr = servsock.accept()
                except socket.timeout:
                    continue
                else:
                    srvsock.close()
                    print "Incoming connection from %s:%s" % addr
                    data = conn.recv(8192)
                    print "Received message from client: %s" % data
                    conn.sendall("Proof from Server!")
        elif addr[2] == "C":
            print "I'm the client!"
            print "PRIV_ADDR: %s:%s" % priv_addr
            time.sleep(2)
            clisock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            clisock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            clisock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
            clisock.bind(priv_addr)
            while True:
                try:
                    print "Connecting to %s:%s" % combine
                    clisock.connect(combine)
                except socket.error as e:
                    print e
                    continue
                else:
                    srvsock.close()
                    print "Connected to %s:%s!" % combine
                    clisock.sendall("Proof from Client")
                    data = clisock.recv(8192)
                    print "Received message from server: %s" % data
