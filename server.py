#!/usr/bin/python
'''
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2019 Florian Schmidt <hi@f012.dev>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
'''

import socket

tcpsocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_STREAM)
tcpsocket.bind(("0.0.0.0", 6001))
tcpsocket.listen(1)
tcpsocket.settimeout(30)

print "Bound to 0.0.0.0:6001"
clients = {}

while True:
    try:
        conn, addr = tcpsocket.accept()
    except socket.timeout:
        continue

    print "New connection from %s:%s" % (addr)
    clients[addr] = conn

    if len(clients) == 2:
        print "Sending Connection"
        (addr1, conn1), (addr2, conn2) = clients.items()
        conn1.sendall("%s:%s:S" % addr2)
        conn2.sendall("%s:%s:C" % addr1)
        conn1.close()
        conn2.close()
        clients = {}
